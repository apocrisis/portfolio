
let menuToggle = document.querySelector('.toggle-menu');
let firstLine = document.querySelector('.first');
let secondLine = document.querySelector('.second');
let thirdLine = document.querySelector('.third');
let menu = document.querySelector('#main-nav');

menuToggle.addEventListener('click', (e) => {
  e.preventDefault();
  firstLine.classList.toggle('rotate-first');
  secondLine.classList.toggle('hide-second');
  thirdLine.classList.toggle('rotate-third');
  menu.classList.toggle('display-menu');
});
